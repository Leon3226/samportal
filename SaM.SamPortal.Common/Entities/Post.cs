﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaM.SamPortal.Common.Entities
{
    public class Post
    {
        #region Fields

        private int _Id;

        private string _Text;

        private int _User_Id;

        private DateTime _DateTime;

        private string _Title;

        private int _Rating;

        private string _Author;

        #endregion

        #region Properties

        public int Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        public string Text
        {
            get { return _Text; }
            set { _Text = value; }
        }

        public string Author
        {
            get { return _Author; }
            set { _Author = value; }
        }

        public int UserId
        {
            get { return _User_Id; }
            set { _User_Id = value; }
        }

        public DateTime DateTime
        {
            get { return _DateTime; }
            set { _DateTime = value; }
        }

        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }

        public int Rating
        {
            get { return _Rating; }
            set { _Rating = value; }
        }

        #endregion

        public Post(int id, string text, int userId, DateTime dateTime, string title, int rating)
        {
            _Id = id;
            _Text = text;
            _User_Id = userId;
            _DateTime = dateTime;
            _Title = title;
            _Rating = rating;
        }
    }
}