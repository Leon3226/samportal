﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaM.SamPortal.Common.Entities
{
    public class LightUser
    {
        private string _CustomNickname;
        private string _Login;
        private int _Id;

        public string CustomNickname
        {
            get { return _CustomNickname; }
            set { _CustomNickname = value; }
        }

        public string Login
        {
            get { return _Login; }
            set { _Login = value; }
        }

        public int Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        public static LightUser ToLightUser(User user)
        {
            return new LightUser(user.CustomNickname, user.Login, user.Id);
        }

        public LightUser(string customNickname, string login, int id)
        {
            _CustomNickname = customNickname;
            _Login = login;
            _Id = id;
        }
    }
}