﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaM.SamPortal.Common.Entities
{
    public class Commentary
    {
        #region Fields

        private int _Id;

        private int _Rating;

        private string _Text;

        private int _User_Id;

        private DateTime _DateTime;

        private int _Post_Id;

        #endregion

        #region Properties

        public int Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        public int Rating
        {
            get { return _Rating; }
            set { _Rating = value; }
        }

        public string Text
        {
            get { return _Text; }
            set { _Text = value; }
        }

        public int UserId
        {
            get { return _User_Id; }
            set { _User_Id = value; }
        }

        public DateTime DateTime
        {
            get { return _DateTime; }
            set { _DateTime = value; }
        }

        public int PostId
        {
            get { return _Post_Id; }
            set { _Post_Id = value; }
        }

        #endregion

        public Commentary(int id, int rating, string text, int userId, DateTime dateTime, int postId)
        {
            _Id = id;
            _Rating = rating;
            _Text = text;
            _User_Id = userId;
            _DateTime = dateTime;
            _Post_Id = postId;
        }
    }
}