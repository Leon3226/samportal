﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;

namespace SaM.SamPortal.Common
{
    public class User
    {
        #region Fields

        private int _Id;

        private int _Rating;

        //private Bitmap _Avatar;

        private string _Email;

        private string _Login;

        private string _Password;

        private string _CustomNickname;

        private bool? _Sex;

        #endregion

        #region Properties

        public int Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        public int Rating
        {
            get { return _Rating; }
            set { _Rating = value; }
        }

        //public Bitmap Avatar
        //{
        //    get { return _Avatar; }
        //    set { _Avatar = value; }
        //}

        public string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }

        public string Login
        {
            get { return _Login; }
            set { _Login = value; }
        }

        public string Password
        {
            get { return _Password; }
            set { _Password = value; }
        }

        public string CustomNickname
        {
            get
            {
                if (_CustomNickname != string.Empty)
                    return _CustomNickname;
                else
                    return Login;

            }
            set { _CustomNickname = value; }
        }

        public bool? Sex
        {
            get { return _Sex; }
            set { _Sex = value; }
        }

        #endregion

        public User(int? id = null, int? rating = null, string email = null, string login = null, string password = null, string customNickname = null, bool? sex = null)
        {
            if (id != null)
            {
                Id = id.Value;
            }
            if (rating != null)
            {
                Rating = rating.Value;
            }
            Email = email;
            Login = login;
            Password = password;
            CustomNickname = customNickname;
            Sex = sex;
        }
    }
}