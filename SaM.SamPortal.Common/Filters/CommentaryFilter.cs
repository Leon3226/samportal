﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services.Description;
using System.Web.UI.WebControls;
using SaM.SamPortal.Common.Filters;

namespace SaM.SamPortal.Common.Filters
{
    public class CommentaryFilter : Filter
    {
        public int? ID;
        public int? FromRating;
        public int? ToRating;
        public string Text;
        public string AuthorName;
        public int? AuthorID;
        public DateTime? FromDateTime;
        public DateTime? ToDateTime;
        public int? PostID;
        public string PostName;
        public int? PostAuthorId;
        public string PostAuthorName;



        public CommentaryFilter(int? id = null, int? fromRating = null, int? toRating = null, string text = null, string authorName = null, int? authorId = null, DateTime? fromDateTime = null, DateTime? toDateTime = null, int? postId = null, string postName = null, int? postAuthorId = null, string postAuthorName = null, int page = 0, int pageSize = 0, OrderBy sortDirection = OrderBy.Ascending)
        {
            Page = page;
            PageSize = pageSize;
            SortDirection = sortDirection;
            ID = id;
            FromRating = fromRating;
            ToRating = toRating;
            Text = text;
            AuthorName = authorName;
            AuthorID = authorId;
            FromDateTime = fromDateTime;
            ToDateTime = toDateTime;
            PostID = postId;
            PostName = postName;
            PostAuthorId = postAuthorId;
            PostAuthorName = postAuthorName;
        }
    }
}