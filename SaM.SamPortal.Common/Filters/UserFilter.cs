﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using SaM.SamPortal.Common.Filters;

namespace SaM.SamPortal.Common.Filters
{
    public class UserFilter : Filter
    {
        public int? ID;
        public int? RatingFrom;
        public int? RatingTo;
        public string Email;
        public string Password;
        public string CustomNickname;
        public string Login;
        public bool? Sex;


        public UserFilter(int? id = null, int? ratingFrom = null, int? ratingTo = null, string email = null, string password = null, string customNickname = null, string login = null, bool? sex = null, int page = 0, int pageSize = 0, OrderBy sortDirection = OrderBy.Ascending)
        {
            Page = page;
            PageSize = pageSize;
            SortDirection = sortDirection;
            ID = id;
            RatingFrom = ratingFrom;
            RatingTo = ratingTo;
            Email = email;
            Password = password;
            CustomNickname = customNickname;
            Login = login;
            Sex = sex;
        }
    }
}