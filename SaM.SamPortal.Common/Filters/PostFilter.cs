﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using SaM.SamPortal.Common.Filters;

namespace SaM.SamPortal.Common.Filters
{
    public class PostFilter : Filter
    {
        public int? ID;
        public int? FromRating;
        public int? ToRating;
        public string Title;
        public DateTime? FromDateTime;
        public DateTime? ToDateTime;
        public int? AuthorID;
        public string AuthorName;
        public string Text;

        public PostFilter(int? id = null, int? fromRating = null, int? toRating = null, string title = null, DateTime? fromDateTime = null, DateTime? toDateTime = null, int? authorId = null, string authorName = null, string text = null, int page = 0, int pageSize = 0, OrderBy sortDirection = OrderBy.Ascending)
        {
            Page = page;
            PageSize = pageSize;
            SortDirection = sortDirection;
            ID = id;
            FromRating = fromRating;
            ToRating = toRating;
            Title = title;
            FromDateTime = fromDateTime;
            ToDateTime = toDateTime;
            AuthorID = authorId;
            AuthorName = authorName;
            Text = text;
        }
    }
}