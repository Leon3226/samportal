﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace SaM.SamPortal.Common.Filters
{
    public abstract class Filter
    {
        public int PageSize { get; set; }
        public int Page { get; set; }
        public OrderBy SortDirection { get; set; }
 
    }
}
