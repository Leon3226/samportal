﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SaM.SamPortal.Common.Filters;
using SaM.SamPortal.DataAccess;
using SaM.SamPortal.Common.Entities;
using Commentary = SaM.SamPortal.Common.Entities.Commentary;

namespace SaM.SamPortal.Business
{
    public class CommentaryLogic : IDataLogic<Commentary, CommentaryFilter>
    {
        private readonly CommentaryProvider _provider;

        public CommentaryLogic()
        {
            _provider = new CommentaryProvider();
        }

        public Commentary GetById(int id)
        {
            var dbPost = _provider.GetById(id);
            if (dbPost == null)
                return null;
            return new Commentary(dbPost.Id, dbPost.Rating, dbPost.Text, dbPost.User_Id, dbPost.DateTime, dbPost.Post_Id);
        }

        public void Create(Commentary item)
        {
            _provider.Create(new DataAccess.Commentary()
            {
                DateTime = item.DateTime,
                Id = item.Id,
                Rating = item.Rating,
                Text = item.Text,
                Post_Id = item.PostId,
                User_Id = item.UserId
            });
        }

        public List<Commentary> GetByCriteria(CommentaryFilter filter, out int totalRows)
        {
            List<Common.Entities.Commentary> toReturnCommentaries = new List<Commentary>();
            List<DataAccess.Commentary> dbUsers = _provider.GetByCriteria(filter, out totalRows);
            foreach (var dbPost in dbUsers)
            {
                toReturnCommentaries.Add(new Commentary(dbPost.Id, dbPost.Rating, dbPost.Text, dbPost.User_Id, dbPost.DateTime, dbPost.Post_Id));
            }
            return toReturnCommentaries;
        }

        public void Update(Commentary entity)
        {
            _provider.Create(new DataAccess.Commentary()
            {
                DateTime = entity.DateTime,
                Id = entity.Id,
                Rating = entity.Rating,
                Text = entity.Text,
                Post_Id = entity.Id,
                User_Id = entity.UserId
            });
        }

        public void Delete(int id)
        {
            _provider.Delete(id);
        }

        public void Delete(Commentary item)
        {
            if (item != null)
            {
                _provider.Update(new DataAccess.Commentary()
                {
                    DateTime = item.DateTime,
                    Id = item.Id,
                    Rating = item.Rating,
                    Text = item.Text,
                    Post_Id = item.Id,
                    User_Id = item.UserId
                });
            }
        }
    }
}
