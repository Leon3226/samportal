﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SaM.SamPortal.Common.Filters;
using SaM.SamPortal.DataAccess;
using User = SaM.SamPortal.Common.User;

namespace SaM.SamPortal.Business
{
    public class UserLogic : IDataLogic<User, UserFilter>
    {
        private readonly UserProvider _provider;

        public UserLogic()
        {
            _provider = new UserProvider();
        }
        public User GetById(int id)
        {
            var dbUser = _provider.GetById(id);
            if (dbUser == null)
            {
                return null;
            }
            return new User(dbUser.Id, dbUser.Rating, dbUser.Email, dbUser.Login, dbUser.Password, dbUser.CustomNickname, dbUser.Sex);
        }

        public void Create(User item)
        {
           _provider.Create(new DataAccess.User()
           {
               //Avatar = ???
               CustomNickname = item.CustomNickname,
               Email = item.Email,
               Login = item.Login,
               Password = item.Password,
               Rating = item.Rating,
               Sex = item.Sex
           });
        }

        public List<User> GetByCriteria(UserFilter filter, out int totalRows)
        {
            List<Common.User> toReturnUsers = new List<User>();
            List<DataAccess.User> dbUsers = _provider.GetByCriteria(filter, out totalRows);
            foreach (var dbUser in dbUsers)
            {
                toReturnUsers.Add(new User(dbUser.Id, dbUser.Rating, dbUser.Email, dbUser.Login, dbUser.Password, dbUser.CustomNickname, dbUser.Sex));
            }
            return toReturnUsers;
        }

        public void Update(User entity)
        {
            _provider.Update(new DataAccess.User()
           {
               //Avatar = ???
               CustomNickname = entity.CustomNickname,
               Email = entity.Email,
               Login = entity.Login,
               Password = entity.Password,
               Rating = entity.Rating,
               Sex = entity.Sex
           });
        }

        public void Delete(int id)
        {
            _provider.Delete(id);
        }

        public void Delete(User item)
        {
            if (item != null)
            {
                _provider.Delete(new DataAccess.User()
                {
                    //Avatar = ???
                    CustomNickname = item.CustomNickname,
                    Email = item.Email,
                    Login = item.Login,
                    Password = item.Password,
                    Rating = item.Rating,
                    Sex = item.Sex
                });
            }
        }
    }
}
