﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SaM.SamPortal.Common.Filters;
using SaM.SamPortal.DataAccess;
using Post = SaM.SamPortal.Common.Entities.Post;

namespace SaM.SamPortal.Business
{
    public class PostLogic : IDataLogic<Post, PostFilter>
    {
        private readonly PostProvider _provider;

        public PostLogic()
        {
            _provider = new PostProvider();
        }

        public Post GetById(int id)
        {
            var dbPost = _provider.GetById(id);
            if (dbPost == null)
                return null;
            return new Post(dbPost.Id, dbPost.Text, dbPost.User_Id, dbPost.DateTime, dbPost.Title, dbPost.Rating);
        }

        public void Create(Post item)
        {
            _provider.Create(new DataAccess.Post()
            {
                DateTime = item.DateTime,
                Id = item.Id,
                Rating = item.Rating,
                Text = item.Text,
                Title = item.Title,
                User_Id = item.UserId
            });
        }

        public List<Post> GetByCriteria(PostFilter filter, out int totalRows)
        {
            List<Common.Entities.Post> toReturnPosts = new List<Post>();
            List<DataAccess.Post> dbUsers = _provider.GetByCriteria(filter, out totalRows);
            foreach (var dbPost in dbUsers)
            {
                toReturnPosts.Add(new Post(dbPost.Id, dbPost.Text, dbPost.User_Id, dbPost.DateTime, dbPost.Title, dbPost.Rating));
            }
            return toReturnPosts;
        }

        public void Update(Post entity)
        {
            _provider.Update(new DataAccess.Post()
            {
                DateTime = entity.DateTime,
                Id = entity.Id,
                Rating = entity.Rating,
                Text = entity.Text,
                Title = entity.Title,
                User_Id = entity.UserId
            });
        }

        public void Delete(int id)
        {
            _provider.Delete(id);
        }

        public void Delete(Post item)
        {
            if (item != null)
            {
                _provider.Update(new DataAccess.Post()
                {
                    DateTime = item.DateTime,
                    Id = item.Id,
                    Rating = item.Rating,
                    Text = item.Text,
                    Title = item.Title,
                    User_Id = item.UserId
                });
            }
        }
    }
}
