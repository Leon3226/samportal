﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SaM.SamPortal.Common.Filters;

namespace SaM.SamPortal.DataAccess
{
    interface IProvider<T, C> where C : Filter
    {
        T GetById(int id);

        void Create(T item);

        List<T> GetByCriteria(C filter, out int totalRows);

        void Update(T entity);

        void Delete(int id);

        void Delete(T item);
    }
}
