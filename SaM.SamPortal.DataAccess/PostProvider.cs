﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SaM.SamPortal.Common.Filters;

namespace SaM.SamPortal.DataAccess
{
    public class PostProvider : IProvider<Post, PostFilter>
    {
        public Post GetById(int id)
        {
            using (var db = new DataBaseDataContext())
            {
                return db.Posts.FirstOrDefault(x => x.Id == id);
            }
        }

        private int getFreeId()
        {
            using (var db = new DataBaseDataContext())
            {
                try
                {
                    return db.Posts.Select(x => x.Id).Max() + 1;
                }
                catch (Exception e)
                {
                    return 1;
                }
            }
        }

        public void Create(Post item)
        {
            using (var db = new DataBaseDataContext())
            {
                if (item != null)
                {
                    item.Id = getFreeId();
                    db.Posts.InsertOnSubmit(item);
                    db.SubmitChanges();
                }
            }
        }

        public List<Post> GetByCriteria(PostFilter filter, out int totalRows)
        {
            using (var db = new DataBaseDataContext())
            {
                List<Post> toReturnPosts = db.Posts.Skip(filter.Page * filter.PageSize).Take(filter.PageSize > 0 ? filter.PageSize : 9999999).OrderByDescending(x => x.DateTime).Where(x =>
                    (filter.ID == null || filter.ID == x.Id) &&
                    (filter.AuthorID == null || filter.AuthorID == x.User_Id) &&
                    (filter.FromDateTime == null || filter.FromDateTime <= x.DateTime) &&
                    (filter.ToDateTime == null || filter.ToDateTime >= x.DateTime) &&
                    (filter.ToRating == null || filter.ToRating >= x.Rating) &&
                    (filter.FromRating == null || filter.FromRating <= x.Rating) &&
                    (filter.Text == null || filter.Text == x.Text) &&
                    (filter.Title == null || filter.Title == x.Title)
                    ).ToList();
                totalRows = toReturnPosts.Count;
                return toReturnPosts;
            }
        }

        public void Update(Post entity)
        {
            using (var db = new DataBaseDataContext())
            {
                var result = db.Posts.FirstOrDefault(x => x.Id == entity.Id);
                if (result != null)
                {
                    result = entity;
                    db.SubmitChanges();
                }
            }
        }

        public void Delete(int id)
        {
            using (var db = new DataBaseDataContext())
            {
                var result = db.Posts.FirstOrDefault(x => x.Id == id);
                if (result != null)
                {
                    db.Posts.DeleteOnSubmit(result);
                    db.SubmitChanges();
                }
            }
        }

        public void Delete(Post item)
        {
            using (var db = new DataBaseDataContext())
            {
                int rows;
                var result = GetByCriteria(new PostFilter(item.Id, item.Rating, item.Rating, item.Title, item.DateTime, item.DateTime, item.User_Id, null, item.Text, 0, 1), out rows);
                if (result != null && result.Count == 1)
                {
                    Delete(result[0].Id);
                }
            }
        }

    }
}
