﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SaM.SamPortal.Common.Filters;

namespace SaM.SamPortal.DataAccess
{
    public class UserProvider : IProvider<User, UserFilter>
    {
        public User GetById(int id)
        {
            using (var db = new DataBaseDataContext())
            {
                return db.Users.FirstOrDefault(x => x.Id == id);
            }
        }

        private int getFreeId()
        {
            using (var db = new DataBaseDataContext())
            {
                return db.Users.Select(x => x.Id).Max() + 1;

            }
        }

        public void Create(User item)
        {
            using (var db = new DataBaseDataContext())
            {
                if (item != null)
                {
                    item.Id = getFreeId();
                    db.Users.InsertOnSubmit(item);
                    db.SubmitChanges();
                }
            }
        }

        public List<User> GetByCriteria(UserFilter filter, out int totalRows)
        {
            using (var db = new DataBaseDataContext())
            {
                List<User> toReturnUsers = db.Users.Skip(filter.Page * filter.PageSize).Take(filter.PageSize > 0 ? filter.PageSize : 9999999).OrderBy(x => x.Rating).Where(x =>
                    (filter.ID == null || filter.ID == x.Id) &&
                    (filter.CustomNickname == null || x.CustomNickname.Contains(filter.CustomNickname) || x.Login.Contains(filter.CustomNickname)) &&
                    (filter.Email == null || x.Email.Contains(filter.Email)) &&
                    (filter.Login == null || x.Login == filter.Login) &&
                    (filter.Password == null || x.Password == filter.Password) &&
                    (filter.RatingTo == null || filter.RatingTo >= x.Rating) &&
                    (filter.RatingFrom == null || filter.RatingFrom <= x.Rating) &&
                    (filter.Sex == null || filter.Sex == x.Sex)
                    ).ToList();
                totalRows = toReturnUsers.Count;
                return toReturnUsers;
            }
        }

        public void Update(User entity)
        {
            using (var db = new DataBaseDataContext())
            {
                var result = db.Users.FirstOrDefault(x => x.Id == entity.Id);
                if (result != null)
                {
                    result = entity;
                    db.SubmitChanges();
                }
            }
        }

        public void Delete(int id)
        {
            using (var db = new DataBaseDataContext())
            {
                var result = db.Users.FirstOrDefault(x => x.Id == id);
                if (result != null)
                {
                    db.Users.DeleteOnSubmit(result);
                    db.SubmitChanges();
                }
            }
        }

        public void Delete(User item)
        {
            using (var db = new DataBaseDataContext())
            {
                int rows;
                var result = GetByCriteria(new UserFilter(null, item.Rating, item.Rating, item.Email, item.Password, item.CustomNickname, item.Login, item.Sex, 0, 10000), out rows);
                if (result != null && result.Count == 1)
                {
                    Delete(result[0].Id);
                }
            }
        }
    }
}
