﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SaM.SamPortal.Common.Filters;

namespace SaM.SamPortal.DataAccess
{
    public class CommentaryProvider : IProvider<Commentary, CommentaryFilter>
    {
        public Commentary GetById(int id)
        {
            using (var db = new DataBaseDataContext())
            {
                return db.Commentaries.FirstOrDefault(x => x.Id == id);
            }
        }

        private int getFreeId()
        {
            using (var db = new DataBaseDataContext())
            {
                try
                {
                    return db.Commentaries.Select(x => x.Id).Max() + 1;
                }
                catch (Exception e)
                {
                    return 1;
                }
            }
        }

        public void Create(Commentary item)
        {
            using (var db = new DataBaseDataContext())
            {
                if (item != null)
                {
                    item.Id = getFreeId();
                    db.Commentaries.InsertOnSubmit(item);
                    db.SubmitChanges();
                }
            }
        }

        public List<Commentary> GetByCriteria(CommentaryFilter filter, out int totalRows)
        {
            using (var db = new DataBaseDataContext())
            {
                List<Commentary> toReturnComments = db.Commentaries.Skip(filter.Page * filter.PageSize).Take(filter.PageSize > 0 ? filter.PageSize : 9999999).OrderByDescending(x => x.DateTime).Where(x =>
                    (filter.ID == null || filter.ID == x.Id) &&
                    (filter.PostID == null || filter.PostID == x.Post_Id) &&
                    (filter.AuthorID == null || filter.AuthorID == x.User_Id) &&
                    (filter.FromDateTime == null || filter.FromDateTime <= x.DateTime) &&
                    (filter.ToDateTime == null || filter.ToDateTime >= x.DateTime) &&
                    (filter.ToRating == null || filter.ToRating >= x.Rating) &&
                    (filter.FromRating == null || filter.FromRating <= x.Rating) &&
                    (filter.Text == null || filter.Text == x.Text)
                    ).ToList();
                totalRows = toReturnComments.Count;
                return toReturnComments;
            }
        }

        public void Update(Commentary entity)
        {
            using (var db = new DataBaseDataContext())
            {
                var result = db.Commentaries.FirstOrDefault(x => x.Id == entity.Id);
                if (result != null)
                {
                    result = entity;
                    db.SubmitChanges();
                }
            }
        }

        public void Delete(int id)
        {
            using (var db = new DataBaseDataContext())
            {
                var result = db.Commentaries.FirstOrDefault(x => x.Id == id);
                if (result != null)
                {
                    db.Commentaries.DeleteOnSubmit(result);
                    db.SubmitChanges();
                }
            }
        }


        public void Delete(Commentary item)
        {
            using (var db = new DataBaseDataContext())
            {
                int rows;
                var result = GetByCriteria(new CommentaryFilter(item.Id, item.Rating, item.Rating, item.Text, null, item.User_Id, item.DateTime, item.DateTime, item.Post_Id, null, null, null, 0, 1), out rows);
                if (result != null && result.Count == 1)
                {
                    Delete(result[0].Id);
                }
            }
        }
    }
}
