﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SaM.SamPortal.Common.Entities;

namespace SaM.SamPortal.Models
{
    public class FeedPostModel
    {
        public Post Post;

        public FeedPostModel(Post post)
        {
            this.Post = post;
        }
    }
}