﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SaM.SamPortal.Common.Entities;
using Commentary = SaM.SamPortal.Common.Entities.Commentary;
using Post = SaM.SamPortal.Common.Entities.Post;

namespace SaM.SamPortal.Models
{
    public class PostModel
    {
        public Post Post;
        public List<Commentary> Commentaries { get; set; }
        public List<LightUser> Users { get; set; }

        public PostModel(Post post, List<Commentary> commentaries, List<LightUser> users)
        {
            Post = post;
            Commentaries = commentaries;
            Users = users;
        }
    }
}