﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SaM.SamPortal.Common.Entities;

namespace SaM.SamPortal.Models
{
    public class FeedModel
    {
        public List<Post> Posts { get; set; }

        public FeedModel(List<Post> posts)
        {
            Posts = posts;
        }
    }
}