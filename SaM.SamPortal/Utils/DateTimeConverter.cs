﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaM.SamPortal.Utils
{
    public static class DateTimeConverter
    {
        public static string ToRelativeTime(DateTime dateTime)
        {
            string toReturn = string.Empty;
            TimeSpan passedTime = DateTime.Now - dateTime;
            if (passedTime.Days < 1)
            {
                if (passedTime.Hours < 1)
                {
                    if (passedTime.Minutes < 1)
                    {
                        if (passedTime.Seconds < 5)
                        {
                            toReturn = "Только что";
                        }
                        else
                        {
                            toReturn = string.Format("{0} секунд назад", passedTime.Hours);
                        }
                    }
                    else
                    {
                        toReturn = string.Format("{0} минут назад", passedTime.Hours);
                    }
                }
                else
                {
                    toReturn = string.Format("{0} часов назад", passedTime.Hours);
                }
            }
            else if (passedTime.Days == 1)
            {
                toReturn = "Вчера";
            }
            else
            {
                toReturn = string.Format("{0} дней назад", passedTime.Days);
            }
            return toReturn;
        }
    }
}