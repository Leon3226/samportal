﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using SaM.SamPortal.Business;
using SaM.SamPortal.Common.Entities;
using SaM.SamPortal.Common.Filters;
using User = SaM.SamPortal.Common.User;

namespace SaM.SamPortal
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : System.Web.HttpApplication
    {
        private List<int> UserIds;
        private List<int> PostIds;
        private string randomWord(int slog)
        {
            string toReturn = string.Empty;
            char[] gl = new[] { 'e', 'y', 'u', 'i', 'o', 'a' };
            char[] sogl = new[] { 'q', 'w', 'r', 't', 'p', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'z', 'x', 'c', 'v', 'b', 'n', 'm' };
            char[] allLetters = gl.Concat(sogl).ToArray();
            for (int i = 0; i < rand.Next(2, 4); i++)
            {
                toReturn += sogl[rand.Next(0, sogl.Length)];
                if (i == 0)
                {
                    toReturn.ToUpper();
                }
                toReturn += gl[rand.Next(0, gl.Length)];
            }
            return toReturn;
        }

        Random rand = new Random();
        private User RandomUser()
        {
            char[] gl = new[] { 'e', 'y', 'u', 'i', 'o', 'a' };
            char[] sogl = new[] { 'q', 'w', 'r', 't', 'p', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'z', 'x', 'c', 'v', 'b', 'n', 'm' };
            char[] allLetters = gl.Concat(sogl).ToArray();
            string[] mailStrings = new[] { "mail.ru", "yandex.by", "yandex.ru", "gmail.com", "yahoo.com" };
            int rating;
            string email;
            string login;
            string password;
            string customLogin;
            bool sex;

            if (rand.Next(0, 2) == 0)
                sex = true;
            else
                sex = false;

            password = string.Empty;
            for (int i = 0; i < rand.Next(8, 16); i++)
            {
                password += allLetters[rand.Next(0, allLetters.Length)];
            }

            rating = rand.Next(0, 100000);

            login = string.Empty;
            for (int i = 0; i < rand.Next(2, 4); i++)
            {
                login += sogl[rand.Next(0, sogl.Length)];
                if (i == 0)
                {
                    login.ToUpper();
                }
                login += gl[rand.Next(0, gl.Length)];
            }
            customLogin = login;
            email = login + '@' + mailStrings[rand.Next(0, mailStrings.Length)];
            login += rand.Next(10000).ToString();
            return new User(null, rating, email, login, password, customLogin, sex);
        }

        private Post RandomPost()
        {
            int rating;
            string text;
            DateTime postTime;
            int UserId;
            string title;

            rating = rand.Next(-40, 11000);
            title = string.Empty;
            for (int i = 0; i < rand.Next(1, 4); i++)
            {
                title += randomWord(rand.Next(1, 6));
                title += " ";
            }

            text = string.Empty;
            for (int i = 0; i < rand.Next(30, 500); i++)
            {
                text += randomWord(rand.Next(1, 6));
                text += " ";
            }

            DateTime from = new DateTime(2004, 01, 01, 0, 0, 0);
            DateTime to = DateTime.Now;
            long difference = to.Ticks - from.Ticks;
            int Hours = (int)TimeSpan.FromTicks(difference).TotalHours;

            postTime = new DateTime();
            TimeSpan span = TimeSpan.FromHours(rand.Next(0, Hours));
            postTime = from.Add(span);
            postTime = postTime.AddMinutes(rand.Next(0, 60));
            postTime = postTime.AddSeconds(rand.Next(0, 60));
            int rows;
            List<int> UserIds = new UserLogic().GetByCriteria(new UserFilter(pageSize: 10000), out rows).Select(x => x.Id).ToList();
            UserId = UserIds[rand.Next(0, UserIds.Count)];
            return new Post(0, text, UserId, postTime, title, rating);
        }

        private Commentary RandomComment()
        {
            int rating = 0;
            string text = string.Empty;
            DateTime postTime = DateTime.Now;
            int UserId = 1;
            int PostId = 1;

            for (int i = 0; i < rand.Next(3, 50); i++)
            {
                text += randomWord(rand.Next(1, 6));
                text += " ";
            }

            rating = rand.Next(-250, 500);
            int rows;
            if (UserIds == null)
            {
                UserIds = new UserLogic().GetByCriteria(new UserFilter(pageSize: 1000000), out rows).Select(x => x.Id).ToList();
            }
            if (PostIds == null)
            {
                PostIds = new PostLogic().GetByCriteria(new PostFilter(pageSize: 1000000), out rows).Select(x => x.Id).ToList();
            }

            UserId = UserIds[rand.Next(UserIds.Count)];
            PostId = PostIds[rand.Next(PostIds.Count)];

            DateTime from = new PostLogic().GetById(PostId).DateTime;
            DateTime to = DateTime.Now;
            long difference = to.Ticks - from.Ticks;
            int Hours = (int)TimeSpan.FromTicks(difference).TotalHours;

            postTime = new DateTime();
            TimeSpan span = TimeSpan.FromHours(rand.Next(0, Hours));
            postTime = from.Add(span);
            postTime = postTime.AddMinutes(rand.Next(0, 60));
            postTime = postTime.AddSeconds(rand.Next(0, 60));



            return new Commentary(0, rating, text, UserId, postTime, PostId);
        }
        protected void Application_Start()
        {
           

            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }
    }
}