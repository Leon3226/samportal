﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SaM.SamPortal.Business;
using SaM.SamPortal.Common;
using SaM.SamPortal.Common.Entities;
using SaM.SamPortal.Common.Filters;
using SaM.SamPortal.Models;

namespace SaM.SamPortal.Controllers
{
    public class FeedController : Controller
    {
        //
        // GET: /Feed/

        public ActionResult Index()
        {
            List<Post> posts = new List<Post>();
            int rows;
            posts = new PostLogic().GetByCriteria(new PostFilter(pageSize: 20), out rows);

            UserLogic userLogic = new UserLogic();
            List<User> users = new List<User>();
            posts.ForEach(x => x.Author = userLogic.GetByCriteria(new UserFilter(id: x.UserId), out rows).FirstOrDefault().CustomNickname);

            return View(new FeedModel(posts));
        }

    }
}
