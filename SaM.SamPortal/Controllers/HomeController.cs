﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SaM.SamPortal.Business;
using SaM.SamPortal.Common;
using SaM.SamPortal.Common.Entities;
using SaM.SamPortal.Common.Filters;
using SaM.SamPortal.Models;

namespace SaM.SamPortal.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        public HomeController()
        {

        }

        public ActionResult Index()
        {
            PostModel postModel;

            UserLogic userLogic = new UserLogic();
            CommentaryLogic  commentaryLogic = new CommentaryLogic();
            PostLogic postLogic = new PostLogic();

            int rows;
            
            Post post = postLogic.GetById(28288);
            List<Commentary> comments = commentaryLogic.GetByCriteria(new CommentaryFilter(pageSize: 100000, postId: post.Id), out rows);
            List<User> users = new List<User>();
            comments.ForEach(x => users.Add(userLogic.GetByCriteria(new UserFilter(pageSize: 1000000, id: x.UserId), out rows).FirstOrDefault()));
            List<LightUser> Lightusers = users.Select(x => LightUser.ToLightUser(x)).ToList();

            postModel = new PostModel(post, comments, Lightusers);
            return View(postModel);
        }

    }
}
